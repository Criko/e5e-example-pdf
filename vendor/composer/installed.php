<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '7312ab239157c74bbdbd9804f5b21c7eab8f6a25',
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '7312ab239157c74bbdbd9804f5b21c7eab8f6a25',
            'dev_requirement' => false,
        ),
        'tecnickcom/tcpdf' => array(
            'pretty_version' => '6.5.0',
            'version' => '6.5.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../tecnickcom/tcpdf',
            'aliases' => array(),
            'reference' => 'cc54c1503685e618b23922f53635f46e87653662',
            'dev_requirement' => false,
        ),
    ),
);
