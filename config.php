<?php
define('EMAIL_HOST', getenv('EMAIL_HOST'));
define('EMAIL_USERNAME', getenv('EMAIL_USERNAME'));
define('EMAIL_PASSWORD', getenv('EMAIL_PASSWORD'));
define('EMAIL_FROM', getenv('EMAIL_FROM'));
define('EMAIL_PORT',getenv('EMAIL_PORT'));
define('EMAIL_SUBJECT',getenv('EMAIL_SUBJECT'));
define('EMAIL_RECIPIENT',getenv('EMAIL_RECIPIENT'));
