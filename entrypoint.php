<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'vendor/autoload.php';
include_once("config.php");

/**
 * @param $event
 * @param $context
 * @return array
 */
function main($event, $context)
{
    // Load autoloader (using Composer)
    require __DIR__ . '/vendor/autoload.php';
    $pdf = new TCPDF();                 // create TCPDF object with default constructor args
    $pdf->AddPage();                    // pretty self-explanatory
    $pdf->Write(1, 'Hello world');      // 1 is line height

    $pdf->Output('hello_world.pdf',"D");
}
